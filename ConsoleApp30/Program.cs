﻿using System;

namespace ConsoleApp30
{
    class Program
    {
        public static int ZjistiPocetStudentu()
        {
            string sPocetStudentu = "";
            int PocetStudentu = 0;
            bool ZadanoDobre = false;
            while (!ZadanoDobre)
            {
                Console.Write("Kolik chces mit studentu: ");
                sPocetStudentu = Console.ReadLine();
                
                if (int.TryParse(sPocetStudentu, out PocetStudentu) && PocetStudentu > 0 )
                {
                    ZadanoDobre = true;
                }
                else
                {
                    Console.WriteLine("Chybne zadani, zadej znovu");
                    ZadanoDobre = false;
                }
            }
            return PocetStudentu;
        }

        private static void VypisSeznam(SeznamStudentu Seznam)
        {
            foreach (var student in Seznam.Seznam)
            { Console.WriteLine(student.Jmeno + " " + student.Prijmeni); }
        }

        static void Main(string[] args)
        {
            int PocetStudentu = 0;
            var Seznam = new SeznamStudentu();
            PocetStudentu = ZjistiPocetStudentu();
            Console.WriteLine("Zadal jsi " + PocetStudentu + " studentu.");
            Seznam.Generuj(PocetStudentu);
            VypisSeznam(Seznam);
            Console.WriteLine("Konec seznamu studentu, zmackni klavesu");
            Console.ReadLine();
        }


        /*Napište program, který se po spuštění uživatele zeptá, kolik chce mít ve třídě studentů. Poté program přečte uživatelský vstup, a pokud uživatel zadá cokoliv jiného, než celé kladné číslo, program ho upozorní na neplatný vstup a zopakuje výzvu.
Po zadání celého kladného čísla program vypíše na obrazovku příslušný počet náhodně vygenerovaných studentů. Každý student má jméno a příjmení. Pro příjmení i jméno platí následující podmínky:
- Má minimálně 3 znaky
- Má maximálně 7 znaků.
- Střídají se v něm samohlásky a souhlásky (čitelnost)
- Můžou začít jak samohláskou, tak souhláskou
- První písmeno je velké

Příklad:
“Kolik chcete mít ve třídě studentů?”
“Ahoj”
“Neplatný vstup, zadejte prosím celé kladné číslo.”
“Kolik chcete mít ve třídě studentů?”
“2”
“Alo Budesam”
“Molus Akaver”*/
    }
}
