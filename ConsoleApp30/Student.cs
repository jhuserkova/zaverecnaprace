﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp30
{
    class Student
    {
        private string samohlasky = "aeiouy";
        private string souhlasky = "bcdfghjklmnpqrstvwxz";
        private int minimalniPocet = 3;
        private int maximalniPocet = 7;

        
        public string Jmeno { get; set; }
        public string Prijmeni { get; set; }

        public Student()
        {
            Jmeno = generuj();
            Prijmeni = generuj();
        }

        private string generuj()        
        {
            string text = "";
            var random = new Random();
            int PocetPismen = random.Next(minimalniPocet, maximalniPocet + 1);
            string sadaPismen1 = samohlasky;
            string sadaPismen2 = souhlasky;

            int NahodnyZacatek = random.Next(0, 10);
            if (NahodnyZacatek % 2 == 0)
            {
                sadaPismen1 = souhlasky;
                sadaPismen2 = samohlasky;
            }
          

            for (int i = 0; i<PocetPismen; i++)
            {
                if (i%2 ==0)
                {
                    int NahodneVybranyIndex = random.Next(0, sadaPismen1.Length - 1);
                    if (i == 0)
                        text += sadaPismen1[NahodneVybranyIndex].ToString().ToUpper();
                    else
                        text += sadaPismen1[NahodneVybranyIndex];                    
                }
                else
                {
                    int NahodneVybranyIndex = random.Next(0, sadaPismen2.Length - 1);
                    if (i == 0)
                        text += sadaPismen2[NahodneVybranyIndex].ToString().ToUpper();
                    else
                        text += sadaPismen2[NahodneVybranyIndex];
                }
            }

            return text;
        }
    }
}

